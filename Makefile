all:
	@echo "Ne fais rien pour éviter de tout compiler par inadvertance\nUtilisez \"make main\" pour compiler le complet"

calculID:
	@gcc -c -g calculID.c -o ./build/calculID.o

conversion:
	@gcc -c -g conversion.c -o ./build/conversion.o

couperIP:
	@gcc -c -g couperIP.c -o ./build/couperIP.o

decoderip:
	@gcc -c -g decoderip.c -o ./build/decoderip.o

verifierIP:
	@gcc -c -g verifierIP.c -o ./build/verifierIP.o

#On lie le tout
main: conversion couperIP decoderip verifierIP calculID # Compile le complet
	@gcc main.c ./build/*.o -o ./AnalyseTrameIP
	@chmod 755 ./AnalyseTrameIP
	@echo "Normalement tout s'est bien passé vous trouverez le programme compilé içi: ./AnalyseTrameIP"

clean:
	@rm -r -f ./build/
	@mkdir ./build
	@rm -f AnalyseTrameIP
	@clear
	@echo "Clean completed"
