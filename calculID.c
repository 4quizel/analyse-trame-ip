#include <stdio.h>
#include <stdlib.h>

void calculID(int a, int b, int c, int d, int mask){
  switch (mask) {
    case 8:
      printf("NetID : %d.0.0.0\n", a);
      printf("HostID : 0.%d.%d.%d\n", b, c, d);
    break;
    case 16 :
      printf("NetID : %d.%d.0.0\n", a, b);
      printf("HostID : 0.0.%d.%d\n", c, d);
    break;
    case 24 :
      printf("NetID : %d.%d.%d.0\n", a, b, c);
      printf("HostID : 0.0.0.%d\n", d);
    case 32 :
      printf("NetID : %d.%d.%d.%d\n", a, b ,c, d);
      printf("HostID : 0.0.0.0\n");
    break;
  }
}
