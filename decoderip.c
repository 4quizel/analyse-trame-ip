#include <stdio.h>

void classe(int a) {

	if (a >= 0 && a <= 255) {
		printf("L'adresse IP fait partie de la classe ");
		if (a >= 0 && a <= 127) {
			printf("A\n");
		}
		else if (a >= 128 && a <= 191) {
			printf("B\n");
		}
		else if (a >= 192 && a <= 223) {
			printf("C\n");
		}
		else if (a >= 224 && a <= 239) {
			printf("D\n");
		}
		else {
			printf("E\n");
		}
	}
	else {
		printf("L'adresse IP ne fait partie de la classe A,B,C,D,E ou est hors [0;255]\n");
	}
}

void privee(int a,int b) {

	printf("L'adresse IP est");
	if (a == 10) {
		printf(" privé\n");
	}
	else if (a == 172 && b >= 16 && b <= 31) {
			printf(" privé\n");
		}
		else if (a == 192 && b == 168) {
				printf(" privé\n");
			}
			else if (a == 255 && b == 255) {
					printf(" l'adresse de diffusion(broadcast)\n");
				}
				else if (a == 240 && b == 0 || a == 239 && b == 255) {
						printf(" l'adresse multicast\n");
					}
					else {
						printf(" public\n");
					}
}

void main() {

	classe(0);
	privee(0,1);
	classe(10);
	privee(10,50);
	classe(172);
	privee(172,17);
	classe(230);
	classe(250);
	classe(256);
	classe(192);
	privee(192,168);
	classe(-1);
	privee(255,255);
	privee(240,0);
	privee(239,255);
}
