#include <stdio.h>
#include <stdlib.h>


int octetA[7], octetB[7], octetC[7], octetD[7], tabMask[31], tabIP[31], tabBinNetID[31], tabBinHostID[31],
    octetNetA, octetNetB, octetNetC, octetNetD, octetHostA ,octetHostB, octetHostC ,octetHostD;

int pows(int a, int b) {
	if(b == 0){
		return 1;
	} else {
		return a*pows(a,b-1);
	}
}

void iPbinaire(int a, int b, int c, int d){
  int i;
//Partie a
  for(i=0; a > 0; i++) {
    octetA[i] = a%2;
    a = a/2;
  }
  /*for(i= 7; i >= 0; i--){  affichage
    printf("%d",octetA[i]);
  }*/
//Partie B
  for(i=0; b > 0; i++) {
    octetB[i] = b%2;
    b = b/2;
  }
  /*for(i= 7; i >= 0; i--){  affichage
    printf("%d",octetB[i]);
  }*/

//Partie C
  for(i=0; c > 0; i++) {
    octetC[i] = c%2;
    c = c/2;
  }
  /*for(i= 7; i >= 0; i--){  affichage
    printf("%d",octetC[i]);
  }*/

//Partie D
  for(i=0; d > 0; i++) {
    octetD[i] = d%2;
    d = d/2;
  }
  /*for(i= 7; i >= 0; i--){  affichage
    printf("%d",octetD[i]);
  }*/
// printf("\n");
}

void maskBinaire(int mask){
  int i;
  for (i = 0; i < 31 - mask ; i++) {
    tabMask[i] = 0;
  }
  for (i = i+1; i <= 31; i++) {
    tabMask[i] = 1;
  }
/*  printf("\n");
  for(int i=31; i >= 0; i--){  affichage
    printf("%d", tabMask[i]);
  }*/
}

void calculIP(int octetA[], int octetB[], int octetC[], int octetD[], int tabMask[]){
  int j = 0;
  for (int a = 0; a < 8; a++) {
    tabIP[j] = octetA[a];
    //printf("a %d j %d\n", a, j);
    j++;
  }
  for (int b = 0; b < 8; b++) {
    tabIP[j] = octetB[b];
    //printf("b %d j %d\n", b, j);
    j++;
  }
  for (int c = 0; c < 8; c++) {
    tabIP[j] = octetA[c];
    //printf("c %d j %d\n", c, j);
    j++;
  }
  for (int d = 0; d < 8; d++) {
    tabIP[j] = octetA[d];
    //printf("d %d j %d\n", d, j);
    j++;
  }
  /*printf("tabIP ");
  for(int i=31; i >= 0; i--){  affichage
    printf("%d", tabIP[i]);
  }
  printf("\n");*/
}

void autreMask(int tabIP[], int tabMask[]){
  for (int i = 31; i >= 0; i--) {
    if (tabIP[i] == 1 && tabMask[i] == 1) {
      tabBinNetID[i] = 1;
    }else{
      tabBinNetID[i] = 0;
    }
  }
  for (int i = 31; i >= 0 ; i--) {
    if (tabBinNetID[i] == 1) {
      tabBinHostID[i] = 0;
    }else{
      tabBinHostID[i] = 1;
    }
  }
  printf("netID :  ");
  for(int i=31; i >= 0; i--){  //affichage
    printf("%d", tabBinNetID[i]);
  }
  printf("\n");
  printf("hostID : ");
  for(int i=31; i >= 0; i--){  //affichage
    printf("%d", tabBinHostID[i]);
  }
}

void ipDecimal(int tabBinNetID[], int tabBinHostID[]){
  int j = 0;  // indice de la puissance
  int k = 3; //  indice des tableaux décimal
  octetNetA = 0; octetNetB = 0; octetNetC = 0; octetNetD = 0;
  octetHostA = 0; octetHostB = 0; octetHostC = 0; octetHostD = 0;
  for (int i = 31; i > 23; i--) {
    octetNetA = octetNetA + tabBinNetID[i] * pows(2,j);
    octetHostA = octetHostA + tabBinHostID[i] * pows(2,j);
    j++;
  }
  k--;
  j = 0;
  for (int i = 23; i > 15; i--) {
    octetNetB = octetNetB + tabBinNetID[i] * pows(2,j);
    octetHostB = octetHostB + tabBinHostID[i] * pows(2,j);
    j++;
  }
  k--;
  j = 0;
  for (int i = 15; i > 7; i--) {
    octetNetC = octetNetC + tabBinNetID[i] * pows(2,j);
    octetHostC = octetHostC + tabBinHostID[i] * pows(2,j);
    j++;
  }
  k--;
  j = 0;
  for (int i = 7; i > 0; i--) {
    octetNetD = octetNetD + tabBinNetID[i] * pows(2,j);
    octetHostD = octetHostD + tabBinHostID[i] * pows(2,j);
    j++;
  }
    printf("DecimalNet: %d.%d.%d.%d\n", octetNetA,octetNetB,octetNetC,octetNetD);
	printf("DecimalHost: %d.%d.%d.%d\n", octetHostA,octetHostB,octetHostC,octetHostD);
}


void main(){
  iPbinaire(246, 124, 212, 42);
  maskBinaire(24);
  printf("\n");
  printf("\n");
  calculIP(octetA, octetB, octetC, octetD, tabMask);
  autreMask(tabIP, tabMask);
  ipDecimal(tabBinNetID, tabBinHostID);
}
