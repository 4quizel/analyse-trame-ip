#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "./header/verifierIP.h"
#include "./header/couperID.h"
#include "./header/calculID.h"
#include "./header/decoderip.h"

#define TAILLE_MAX 18

void main() {
  int a, b, c, d, mask;
  int* tabIP;
  char ip[TAILLE_MAX + 1];
  printf("Entrez une adresse ip avec masque dans la forme a.b.c.d\\e : ");
  if(scanf("%s", ip) != 1){
  }
  printf("%s\n", ip);
  tailleIP(ip);
  isDigit(ip);
  verifierSeparateur(ip);
  couperIP(ip, tabIP);

  classe(a);
  privee(a, b);
  calculID(a, b, c, d, mask);

}
