#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#define TAILLE_MAX 18


//vérifier la taille de l'adresse ip (ça marche)
void tailleIP(char *ip){
  if (strlen(ip) < 9 || strlen(ip)> TAILLE_MAX) {
    printf("l'adresse ip doit être de 9 à 18 caractères\n");
    EXIT_FAILURE;
  }
  int a = strlen(ip);
  printf("%d\n", a);
}


void verifierSeparateur(char ip[]){
  int p; //nb de points
  int s; //nb d'antislash
  for (char *t = ip; *t != '\0'; ++t) { //parcour du tableau ip
  //test présence de points
    if (*t == '.'){
      p++;
    }
  //test présence d'antislash
    if  (*t == '\\') {
      s++;
    }
  }
  if (p == 3 && s == 1) { //test bon nb de points et antislash
    printf("Le format est respecté.\n");
  }else{
    printf("Le format n'est pas respecté.\n");
  }
}

//test chiffre
void isDigit(char *ip){
  int verif = 0;
  for (char *t = ip; *t != '\0'; ++t) { //parcour du tableau ip
    //test
    if ( isalpha(*t) != 0 && *t != '.' && *t != '\\' && verif == 0){
      verif = 1;
      printf("il n'y a pas de lettres dans une adresse ip.\n");
    }
  }
  if (verif == 0) {
    printf("aucune lettres trouvé dans cette adresse ip\n");
  }
}
