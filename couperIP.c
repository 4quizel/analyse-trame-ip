#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define TAILLE_MAX 18

void couperIP(char *ip, int* tabIP){

  char ipBis[TAILLE_MAX+1]; //dupliquation de l'adresse ip à manipuler
  strcpy(ipBis, ip);
  int i = 0;
  const char* sep = ".\\";  //separateurs à enlever
  //séparer les octets en les stockant un par un dans un tableau d'int
  char* token = strtok(ipBis, sep); //mémoire tempon de l'octet découpé
  while ( token != NULL ) {
        printf ( "%s\n", token);
        tabIP[i] = atoi(token);
        token = strtok(NULL, sep);
        i++;
      }
}

//test fonction (à enlever à la fin)
void main(int argc, char const *argv[]) {
  char ip[TAILLE_MAX+1];
  printf("Entrez une adresse ip avec masque dans la forme a.b.c.d\\e : ");
  if(scanf("%s", ip) != 1){
  }
  printf("%s\n", ip);
  int tabIP[5];
  couperIP(ip, tabIP);
  for (int i = 0; i < 5; i++) {
    printf("%d\n", tabIP[i]);
  }
}
